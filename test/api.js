const {
    KiniroCore,
    symbol,
    getType,
    unwrap,
    wrap,
    nil,
    parenth,
    cons,
    object,
    listToArray,
    arrayToList,
    seq,
    lift, unlift,
    types,
    keywords
} = require('../index.js');
const parseRaw = require('@kiniro/parser');
const assert = require('assert');

describe('KiniroCore', () => {
    it('is abstract', () => {
        assert.throws(() => new KiniroCore());
    });

    it('accepts async parse function', () => {
        class Kin extends KiniroCore {
            constructor (opts) {
                super();
                this.parseRaw = function() {
                    return new Promise((resolve) => {
                        resolve(parseRaw(...arguments));
                    });
                };
            }
        }

        return new Kin().run('1').then(x => assert.equal(1, unlift(x)));
    });

    it('type constants are all different', () => {
        assert.equal(Object.values(types).filter(function(elem, index, self) {
            return index === self.indexOf(elem);
        }).length, Object.keys(types).length);
    });


    it('symbol constants are all different', () => {
        let tcs = Array.from(keywords.values());
        assert.equal(tcs.filter(function(elem, index, self) {
            return index === self.indexOf(elem);
        }).length, Array.from(keywords.values()).length);
    });
});

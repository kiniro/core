![npm bundle size (minified)](https://img.shields.io/bundlephobia/min/@kiniro/core.svg)
![NpmLicense](https://img.shields.io/npm/l/@kiniro/core.svg)

This package contains internals of [@kiniro/lang](https://gitlab.com/kiniro/lang).

Docs are available [here](https://kiniro.gitlab.io/core/).

[gitlab](https://gitlab.com/kiniro/core) / [npm](https://www.npmjs.com/package/@kiniro/core)

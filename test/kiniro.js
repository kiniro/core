const assert = require('assert');
const parseRaw = require('@kiniro/parser');
const prelude = require('@kiniro/prelude');

const {
    KiniroCore,
    symbol,
    getType,
    unwrap,
    wrap,
    nil,
    parenth,
    cons,
    object,
    listToArray,
    arrayToList,
    seq,
    lift, unlift,
    types,
    keywords
} = require('../index.js');

class Kiniro extends KiniroCore {
    constructor () {
        super(...arguments);
        this.parseRaw = parseRaw;
        this.load(prelude);
    }
}

describe('Kiniro', () => {
    describe('.lookup()', () => {
        it('allows to retrieve values defined programmically', async () => {
            const kiniro = new Kiniro();
            await kiniro.run('(def x 1)');
            assert.equal(kiniro.lookup('x'), 1);
            assert.throws(() => kiniro.lookup('y'), x => x == "Couldn't lookup: y");
        });
    });

    describe('.define()', () => {
        it('allows values to be accessed programmically', async () => {
            const kiniro = new Kiniro();
            kiniro.define('x', 2);
            const res = await kiniro.run('(+ x 1)');
            assert.equal(unlift(res), 3);
        });

        it('allows to modify a value', async () => {
            const kiniro = new Kiniro();
            kiniro.define('x', 0);
            await kiniro.run('(def x 1)');
            kiniro.define('x', 2);
            assert.equal(kiniro.lookup('x'), 2);
            assert.equal(unlift(await kiniro.run('x')), 2);
        });

        it('does not share environments between instances', async () => {
            const kiniro1 = new Kiniro();
            kiniro1.define('a', 1);
            const kiniro2 = new Kiniro();
            assert.throws(() => kiniro2.lookup('a'));
            kiniro2.define('a', 2);
            assert.equal(kiniro1.lookup('a'), 1);
            assert.equal(kiniro2.lookup('a'), 2);
        });
    });

    describe('.exec()', () => {
        it('returns unlifted value', async () => {
            const res = await new Kiniro().exec('1');
            assert.equal(res, 1);
        });
    });


    describe('.run()', () => {
        it('returns lifted value', async () => {
            const res = await new Kiniro().run('1');
            assert.deepStrictEqual(res, lift(1));
        });
    });

    describe('.load()', () => {
        it('does not throw if object is ill-formed', () => {
            assert.doesNotThrow(() => {
                const kiniro = new Kiniro();
                kiniro.load({});
            });
        });

        it('populates definitions (raw)', () => {
            const kiniro = new Kiniro();
            kiniro.load({raw: {a:1}});
            assert.equal(kiniro.lookup('a'), 1);
        });

        it('populates definitions (lifted)', () => {
            const kiniro = new Kiniro();
            kiniro.load({lifted: {a:1}});
            assert.equal(kiniro.lookup('a'), 1);
        });


        it('populates definitions (proxyMap)', () => {
            const kiniro = new Kiniro();
            assert.equal(kiniro.proxyMap.has(kiniro), false);
            kiniro.load({proxyMap: new Map([[kiniro, 1]])});
            assert.equal(kiniro.proxyMap.has(kiniro), true);
        });

        it('accepts function as argument', () => {
            const kiniro = new Kiniro();
            kiniro.load(x => {
                assert.deepStrictEqual(x, kiniro);
                return {raw: {a:1}};
            });
            assert.equal(kiniro.lookup('a'), 1);
        });
    });
});

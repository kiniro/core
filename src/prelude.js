/** Object containing default definitions. */
const prelude = {
    lifted: {
        do: function () {
	    return arguments[arguments.length - 1];
        },

        cons,

        empty: function (sth) {
            return wrap(getType(sth) === NIL);
        },

        list: function () {
            return arrayToList(Array.from(arguments));
        },

        map: async function map (f, lst) {
            const type = getType(lst);
            const value = unwrap(lst);

            if (NIL !== type && CONS !== type) {
                throw '[map]: not a list';
            }

            if (type === NIL) {
                return nil();
            } else {
                return cons(await unwrap(f)(value[0]), await map(f, value[1]));
            }
        },

        head: function (lst) {
            const type = getType(lst);

            if (type === CONS) {
                return unwrap(lst)[0];
            }
            if (type === NIL) {
                throw '[head] head of nil!';
            }

            throw '[head] not a list!';
        },

        tail: function (lst) {
            if (getType(lst) === CONS) {
                return unwrap(lst)[1];
            }

            throw '[tail] not a CONS!';
        }
    },

    raw: {
        null: null
    },
};

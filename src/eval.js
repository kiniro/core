const evalFunction = function (expr, env, done, error) {

    const type = getType(expr);
    const value = unwrap(expr);

    if (WRAPPED === type || NIL === type || CONS === type || OBJECT === type) {
        return done(expr);
    }

    if (SYMBOL === type) {
        return done(env.lookup(value));
    }

    if (PARENTH !== type) {
        return error(ERR_ILLEGAL_TYPE);
    }

    if (!value.length)
        return error(ERR_EMPTY_PARENTHESES);

    if (SYMBOL === getType(value[0])) {
        const first = unwrap(value[0]);

        if (KEYWORD_BEGIN === first) {
            return this.evalMany(value.slice(1), env.clone())
            // .pop() is safe because of validate()
                .then(x => done(x.pop())).catch(error);
        }

        if (KEYWORD_LAMBDA === first) {
            // Extract argument names
            const argNames = unwrap(value[1]).map(unwrap);
            return done(wrap((function () {
                // NOTE: arguments are already evaluated

                const nenv = env.clone();
                const args = Array.from(arguments);
                const argsLength = argNames.length;

                // This check is needed because arrayToList is quite expensive.
                if (!argNames.includes(KEYWORD_ARGS))
                    nenv.define(KEYWORD_ARGS, arrayToList(args));

                args.forEach((arg, i) =>
                             argsLength <= i || nenv.define(argNames[i], arg));


                return this.evalMany(value.slice(2), nenv).then(last);
            }).bind(this)));
        }

        if (KEYWORD_LET === first) {
            const iterate = (vars, env) => {
                if (vars.length) {
                    const exprs = unwrap(vars[0]);
                    const name = unwrap(exprs[0]);

                    return this.evalMany(exprs.slice(1), env)
                        .then(values => {
                            const value = last(values);
                            env.define(name, value);
                            iterate(vars.slice(1), env);
                        })
                        .catch(error);
                } else {
                    return this.eval(value[2], env)
                        .then(done)
                        .catch(error);
                }
            };

            return iterate(unwrap(value[1]), env.clone());
        }

        if (KEYWORD_SET === first) {
            if (value.length === 3) {
                return this.eval(value[2], env).then(evaled => {
                    env.set(unwrap(value[1]), evaled);
                    done(evaled);
                }).catch(error);
            } else if (value.length === 4) {
                return this.eval(value[1], env).then(obj => {
                    this.eval(value[2], env).then(prop => {
                        this.eval(value[3], env).then(result => {
                            const type = getType(obj);
                            const propValue = unwrap(prop);
                            const objValue = unwrap(obj);

                            if (type === OBJECT) {
                                objValue[propValue] = result;
                                done(obj);
                            } else if (type === WRAPPED && typeof objValue === 'object') {
                                objValue[propValue] = unlift(result, this.proxy);
                                done(obj);
                            } else {
                                throw '[set]: not an object';
                            }
                        }).catch(error);
                    }).catch(error);
                }).catch(error);
            }
        }

        if (KEYWORD_IF === first) {
            return this.eval(value[1], env).then(evaled => {
                const type = getType(evaled);
                const ix = (WRAPPED === type && unwrap(evaled) || CONS === type || NIL === type) ? 2 : 3;
                this.eval(value[ix], env).then(done).catch(error);
            }).catch(error);
        }

        if (KEYWORD_DEFINE === first) {
            if (value.length === 4) {
                // Construct a new lambda
                const lam = [];
                lam.push(symbol(KEYWORD_LAMBDA));

                // Add argument list
                lam.push(value[2]);

                // Add body
                lam.push(value[3]);

                return this.eval(parenth(lam), env)
                    .then(evaled => {
                        env.define(unwrap(value[1]), evaled);
                        done(nil());
                    })
                    .catch(error);
            } else if (value.length === 3) {
                // Define a variable
                return this.eval(value[2], env)
                    .then(evaled => {
                        env.define(unwrap(value[1]), evaled);
                        done(nil());
                    })
                    .catch(error);
            } else {
                error('define: ' + ERR_INCORRECT_SYNTAX);
            }
        }

        if (KEYWORD_COND === first) {
            let condExpr = parenth([symbol(KEYWORD_THROW), wrap(ERR_COND)]);

            for (let pair of value.slice(1).reverse()) {

                const [cond, res] = unwrap(pair);
                condExpr = parenth([symbol(KEYWORD_IF), cond, res, condExpr]);
            }

            return this.eval(condExpr, env)
                .then(done)
                .catch(error);
        }

        if (KEYWORD_APPLY === first) {
            return this.eval(value[2], env)
                .then(argList =>
                      this.eval(parenth([value[1], ...listToArray(argList)]), env)
                      .then(done).catch(error))
                .catch(error);
        }

        if (KEYWORD_THROW === first) {
            return this.eval(value[1], env)
                .then(value => error(unwrap(value) || nil()))
                .catch(error);
        }

        if (KEYWORD_TRY === first) {
            let caught = false;
            try {
                return this.eval(value[1], env)
                    .catch(err => {
                        caught = true;
                        return this.eval(parenth([value[2], wrap(err)]), env).then(done).catch(error);
                    });
            } catch (err) {
                if (caught) {
                    return error(err);
                } else {
                    return this.eval(parenth([value[2], wrap(err)]), env).then(done).catch(error);
                }
            }

        }

        if (KEYWORD_PAR === first) {
            return Promise.all(value.slice(1).map(expr =>
                                                  this.eval(expr, env)))
                .then(arrayToList)
                .then(done)
                .catch(error);
        }

        if (KEYWORD_ASYNC === first) {
            return done(wrap(new Promise((resolve, reject) =>
                                         this.evalMany(value.slice(1), env)
                                             .then(last)
                                             .then(resolve).catch(reject))));
        }

        if (KEYWORD_AWAIT === first) {
            return this.eval(value[1], env).then(result => {
                result = unwrap(result);
                if (result instanceof Promise) {
                    return result.then(done).catch(error);
                } else {
                    return done(wrap(result));
                }
            }).catch(error);
        }

        if (KEYWORD_QUOTE === first) {
            return done(exprToList(value[1]));
        }

        if (KEYWORD_EVAL === first) {
            return this.eval(value[1], env).then(result => {
                if (getType(result) === CONS) {
                    this.eval(listToExpr(result), env)
                        .then(done).catch(error);
                } else {
                    error('[eval]: not a list');
                }
            }).catch(error);
        }

        if (KEYWORD_DOT === first) {
            return this.eval(value[1], env).then(result => {
                this.eval(value[2], env).then(prop => {
                    const type = getType(result);
                    let resValue = unwrap(result);
                    const typeOfResValue = typeof resValue;

                    // Prop assumed to be a wrapped string.
                    prop = unwrap(prop);

                    if (type === OBJECT) {
                        if (typeof resValue === 'undefined') {
                            return error('Trying to read property of undefined');
                        }
                        if (typeof prop !== 'string') {
                            return error('Property must be a string');
                        }
                        return done(this.proxy(resValue[prop]));
                    } else if (type === WRAPPED && (typeOfResValue === 'object' ||
                                                    typeOfResValue === 'function')) {
                        // If we have a wrapped function, we need to get the raw
                        // (original) value (i.e. result.raw), not the async
                        // function created by 'lift' (i.e. value of unwrap(result)).
                        if ('function' == typeOfResValue) {
                            if (typeof result.raw === 'function') {
                                resValue = result.raw;
                            }
                        }

                        // 'this.proxy' call is needed because later 'res' may
                        // be bound (otherwise unbound value will not be proxified).
                        let res = this.proxy(resValue[prop]);
                        if (typeof res == 'function') {
                            res = res.bind(resValue);
                        }

                        return done(lift(res, this.proxy));
                    }

                    return error(`trying to access property of a non-object`);
                }).catch(error);
            }).catch(error);
        }
    }

    return this.evalMany(value, env).then(evaled => {
        if (WRAPPED === getType(evaled[0])) {
            const fun = unwrap(evaled[0]);

            if (typeof fun === 'function') {
                let result = fun.apply(this, evaled.slice(1));
                if (result instanceof Promise || result instanceof SyncPromise) {
                    return result.then(done).catch(error);
                } else {
                    return done(result);
                }
            }
        }

        return error(ERR_CANT_EVAL);
    }).catch(error);
};

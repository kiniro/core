const objProto = Object.getPrototypeOf({});

/** Convert JS value to kiniro-lang expression */
function lift (value, proxy = x => x, map = new WeakMap()) {
    if (value instanceof Array) {
        return arrayToList(value.map(x => lift(x, proxy, map)));
    } else if ('function' === typeof value) {
        // Apply overrides
        value = proxy(value);
        // Construct a wrapped function
        let wrapped = wrap(async function () {
            return lift(await value.apply(this,
                                          [].map.call(arguments, arg => unlift(arg, proxy))),
                        proxy,
                        map);
        });
        wrapped.raw = value;
        return wrapped;
    } else if (typeof value == 'object' && value !== null) {
        const proto = Object.getPrototypeOf(value);
        if (proto === objProto || proto === null) {
            if (map.has(value)) {
                return [OBJECT, map.get(value)];
            }
            return [OBJECT, Object.getOwnPropertyNames(value).reduce((result, prop) => {
                // No need to call proxy here - it will later be called at some
                // other if/else clause anyway.
                // Also, proxyMap cannot contain 'result' object, because it
                // was constructed only recently.
                map.set(value, result);
                result[prop] = lift(value[prop], proxy, map);
                return result;
            }, Object.create(proto))];
        }
    }
    return wrap(proxy(value));
};

/** Convert kiniro-lang expression to JS value */
function unlift (obj, proxy = x => x, map = new WeakMap()) {
    const type = getType(obj);
    const value = unwrap(obj);
    if (NIL === type)
        return [];
    if (CONS === type) {
        return listToArray(obj).map(x => unlift(x, proxy, map));
    } else if (WRAPPED === type && 'function' === typeof value) {
        if ('function' === typeof obj.raw)
            return proxy(obj.raw);
        return async function () {
            return unlift(await proxy(value).apply(this, [].map.call(arguments, arg => lift(arg, proxy))),
                          proxy,
                          map);
        };
    } else if (OBJECT === type) {
        if (map.has(value)) {
            return map.get(value);
        }
        return Object.keys(value).reduce((result, prop) => {
            map.set(value, result);
            result[prop] = unlift(value[prop], proxy, map);
            return result;
        }, Object.create(Object.getPrototypeOf(value)));
    } else {
        // Proxy call is needed here in order to make possible to override literals
        // (e.g. numbers).
        return proxy(unwrap(obj));
    }
}

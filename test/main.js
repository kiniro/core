const assert = require('assert');
const fs = require('fs');
const parseRaw = require('@kiniro/parser');
const prelude = require('@kiniro/prelude');
const clone = require('clone');

const {
    KiniroCore,
    symbol,
    getType,
    unwrap,
    wrap,
    nil,
    parenth,
    cons,
    object,
    listToArray,
    arrayToList,
    seq,
    lift, unlift,
    types,
    keywords
} = require('../index.js');

function mergePrelude (p1, p2) {
    p1.raw = Object.assign(p1.raw || {}, p2.raw);
    p1.lifted = Object.assign(p1.lifted || {}, p2.lifted);
    p1.code = (p1.code || []).concat(p2.code);
    return p1;
}

class Kiniro extends KiniroCore {
    constructor (opts) {
        opts = opts || {};
        super(opts);
        this.parseRaw = parseRaw;
        this.load(prelude);
    }
}

const shouldFail = (promise, errValidation) => new Promise ((resolve, reject) => {
    errValidation = errValidation || (x => true);

    promise.then(reject).catch(x => {
        if (errValidation(x)) {
            resolve(false);
        } else {
            reject("error is invalid: " + x);
        }
    });
});

const mkPrelude = prelude => {
    if (typeof prelude != 'object') {
        prelude = {};
    }
    prelude.raw = prelude.raw || {};
    prelude.lifted = prelude.lifted || {};
    prelude.code = prelude.code || [];
    return prelude;
};

/** Convert CONS/NIL list to Array with unwrapped elements */
function listToArrayValues (lst) {
    return listToArray(lst).map(unwrap);
}

describe('arrayToList', () => {
    it('should pass tests', async () => {
        const tests = [
            ['[1 2 3]', [1, 2, 3]]
        ];

        for (let [code, res] of tests)  {
            let result = await new Kiniro().run(code);
            assert.deepStrictEqual(result, lift(res));
        }
    });
    it('does not mutate the argument', async () => {
        const tests = [
            [1,2,3],
            lift([1,2,3])
        ];

        tests.forEach(test => {
            let tmp = clone(test);
            arrayToList(test);
            assert.deepStrictEqual(tmp, test);
        });
    });

});

describe('listToArrayValues', () => {
    it('should pass tests', async () => {
        const tests = [
            ['[1 2 3]', [1, 2, 3]]
        ];

        for (let [code, res] of tests)  {
            let result = await new Kiniro().run(code);
            assert.deepStrictEqual(result, lift(res));
        }
    });
});

var tests = [
    // string literals
    [
        "string literals - 1",
        '"string"',
        'string'
    ],

    /* numeric literals */
    [
        "numeric literals - 1",
        '1',
        1
    ],


    [
        "numeric literals - 2",
        '1.123',
        1.123
    ],


    [
        "numeric literals - 3 - negative",
        '-10.123',
        -10.123
    ],


    [
        "numeric literals - 3 - NaN",
        'NaN',
        NaN
    ],

    [
        "numeric literals - 3 - isNaN(NaN)",
        '(isNaN NaN)',
        isNaN(NaN)
    ],

    [
        "numeric literals - 3 - 1 / 0",
        '(isNaN (/ 1 0))',
        isNaN(1/0)
    ],


    [
        "numeric literals - 3 - 1 / 0",
        '(isNaN (/ 1 0))',
        isNaN(1/0)
    ],

    [
        'list literals',
        '[1 2]',
        lift([1,2])
    ],

    ...Object.keys({
        '"': '"',
        '\\': '\\',
        'b': '\b',
        'f': '\f',
        'n': '\n',
        'r': '\r',
        't': '\t'
    }).map(k => {
        const s = eval('"\\' + k + '"');
        return [
            "string literals - escape sequences - \\" + k,
            '"\\' + k + '"',
            s
        ];
    }),

    // function application
    [ "or - 1", '(or 1 0)', true ],
    [ "or - 2", '(or 0 0)', false ],
    [ "or - 3", '(or 0 1)', true ],
    [ "or - 4", '(or 1 1)', true ],
    [ "and - 1", '(and 0 0)', false ],
    [ "and - 2", '(and 1 0)', false ],
    [ "and - 3", '(and 0 1)', false ],
    [ "and - 4", '(and 1 1)', true ],

    // lambda
    [
        "lambda",
        '((\\ y -> (+ 1 y)) 1)',
        2
    ],

    [
        "lambda - multiple bodies",
        '((\\ y -> (+ 1 y) (+ 2 y)) 1)',
        3
    ],

    [
        "let - 1",
        '(let ((x 1)) x)',
        1
    ],

    [
        "let - 2",
        '((let ((x 1)) (\\-> x)))',
        1
    ],

    [
        "nested `let`s - 1",
        '(let ((x 2)) (let ((y 1)) x))',
        2
    ],

    [
        "nested `let`s - 2",
        '(let ((x 2)) (let ((y 1)) (let ((z 3)) x)))',
        2
    ],

    [
        "let - overwriting",
        '(let ((x 1) (y 2) (x (+ x y))) x)',
        3
    ],

    [
        "let - scoping",
        '(let ((x 1))\n\
           (+ (let ((x 2)) x) x))',
        3
    ],

    [
        "variables do not escape lambda",
        '((\\x -> x) 1) x',
        null,
        { error: true }
    ],

    [
        "variables do not escape let bindings",
        '(let ((x 1)) x) x',
        null,
        { error: true }
    ],

    [
        "lambda does not bind 'args' as a list of arguments if there exists an argument with that name",
        '((\\args -> args) 1)',
        1
    ],

    // block-level scoping (begin)
    [
        "begin - variable does not escape begin",
        '(def x 0) (begin (def x 1)) x',
        0
    ],

    [
        "begin - variable escape begin (using set)",
        '(def y 0) (begin (set y 1)) y',
        1
    ],

    [
        "begin - variable does not escape begin (using set after def)",
        '(def x 0) (begin (def x 1) (set x 2)) x',
        0
    ],

    // blocks without separate scope (do)
    [
        "do - variable escapes scope",
        '(def x 0) (do (def x 1)) x',
        1
    ],

    [
        "do - variable escapes scope",
        '(def x 0) (do (set x 1)) x',
        1
    ],

    [
        "do - variable escapes scope",
        '(def x 0) (do (def x 1) (set x 2)) x',
        2
    ],

    // set (assignment operator)
    [
        "set - set changes variable's value",
        '(def x 0) (set x 1)',
        1
    ],

    [
        "set - setting undefined variable throws",
        '(set x 1)',
        null,
        { error: true }
    ],

    [
        "set - object mutation",
        '(def y { a: { b: 0 } })\n\
         (set y.a.b { c: 1 })\n\
         (set y.a "d" 3) y',
        lift({ a: { b: { c: 1 }, d: 3 }})
    ],

    [
        "set - chaining",
        '(def x {})\n\
         (set (set (set x "a" 0) "b" 1) "c" 3) x',
        lift({ a: 0, b: 1, c: 3 })
    ],

    [
        "set - circular references 1",
        '(def x {})\n\
         (set x.x x) x',
        x => assert.deepStrictEqual(unlift(x), (() => {
            const x = {};
            x.x = x;
            return x;
        })())
    ],

    [
        "set - circular references 2",
        '(def x {})\n\
         (set x.x x) x',
        lift((() => {
            const x = {};
            x.x = x;
            return x;
        })())
    ],

    [
        "set - circular references 3",
        '(def x {})\n\
         (set x.x x)\n\
         (set x.y x)\n\
         x',
        lift((() => {
            const x = {};
            x.x = x;
            x.y = x;
            return x;
        })())
    ],

    [
        "set - mutual circular references",
        '(def x {}) (def y {})\n\
         (set x.y y)\n\
         (set y.x x)\n\
         x',
        lift((() => {
            const x = {}, y = {};
            x.y = y;
            y.x = x;
            return x;
        })())
    ],

    // if
    [
        "if - false",
        '(if 0 1 2)',
        2
    ],

    [
        "if - true",
        '(if 1 2 3)',
        2
    ],

    [
        "if - nested 1",
        '(if 1 (if 0 2 3) 4)',
        3
    ],

    // list primitives from prelude
    [
        "empty - 1",
        '(empty [])',
        true
    ],

    [
        "empty - 2",
        '(empty (cons 1 []))',
        false
    ],

    [
        "empty - 3",
        '(empty $ tail [1])',
        true
    ],

    [
        "empty - 4",
        '(empty ())',
        null,
        { error: true }
    ],

    [
        "head - 1",
        '(head (cons 1 []))',
        1
    ],

    [
        "tail - 1",
        '(tail (cons [] 1))',
        1,
    ],

    [
        "tail - 2",
        '(tail [])',
        null,
        { error: true }
    ],

    [
        "tail - 3",
        '(tail [1])',
        nil()
    ],

    [
        "cons",
        '(head (cons 1 2))',
        1
    ],

    [
        "cons",
        '(tail (cons 1 2))',
        2
    ],

    [
        "cons",
        '(let ((l [1 2 3])\n\
               ;; la = [2 3]\n\
               (la (tail l))\n\
               ;; lb = [0 2 3]  \n\
               (lb (cons 0 la)) \n\
               ;; lc = [4 2 3] \n\
               (lc (cons 4 la)))\n\
            [l la lb lc])',
        lift([[1, 2, 3], [2, 3], [0, 2, 3], [4, 2, 3]])
    ],

    [
        "map - basic",
        '(def p1 (x) (+ 1 x))\n\
         (map p1 [1 2 3])',
        lift([2, 3, 4])
    ],

    [
        "map - 2d list",
        '(def p1 (x) (+ 1 x))\n\
         (map (\\x -> (map p1 x)) [[1 2 3] [4 5 6]])',
        lift([[2, 3, 4], [5, 6, 7]])
    ],

    // Apply
    [
        "apply - 1",
        '(apply + [1 2 3])',
        6
    ],
    [
        "apply - 2",
        '(apply + (cons 4 [1 2 3]))',
        10
    ],

    // function as return value
    [
        "higher-order - 1",
        '(def mkPlus ()\n\
           (do\n\
             (def plus (x) (+ 1 x))\n\
             plus))\n\
         ((mkPlus) 0)',
        1
    ],

    [
        "higher-order - 2",
        "(def mkPlus ()\n\
           (do\n\
             (def plus (x) (+ 1 x))\n\
             (\\x -> plus)))\n\
         (((mkPlus)) 0)",
        1
    ],

    // recursive functions
    [
        "recursive - 1",
        '(let (\n\
               (f (\\ x ->\n\
                          (if x (f (- x 1))\n\
                               0)))) (f 3))',
        0
    ],

    // recursive functions on lists
    [
        "length - 1",
        '(let ((len (\\lst ->\n\
                      (if (empty lst)\n\
                          0\n\
                        (+ 1 $ len $ tail lst)))))\n\
           (len $ list 1 2 $ cons 3 []))',
        3
    ],

    [
        "repeat - 1",
        "(let ((repeat\n\
                 (\\x n ->\n\
                    (if n\n\
                        (cons x $ repeat x $ - n 1)\n\
                      []))))\n\
           (repeat 10 10))",
        lift([10, 10, 10, 10, 10, 10, 10, 10, 10, 10])
    ],

    [
        "map - 1",
        '(let ((map (\\f lst -> \n\
            (if (empty lst)\n\
                []\n\
                (cons (f $ head lst)\n\
                    (map f (tail lst))) ))))\n\
            (map (\\x -> (+ 1 x)) (list 0 1 2 3)))',
        lift([1,2,3,4]),
        { log: false }
    ],

    [
        "repeat - 1",
        '(let ((repeat\n\
                 (\\x n -> \n\
                    (if n (cons x (repeat x (- n 1))) [])))\n\
               (length (\\ lst -> (if (empty lst)\n\
                                         0\n\
                                       (+ 1 (length (tail lst)))))))\n\
           (length $ repeat 1 100))',
        100
    ],

    [
        "do - 1",
        '(do 1 2 3)',
        3
    ],

    [
        "def - variable in a variable binding is trying to access itself",
        '(def x x)',
        null,
        { error: true }
    ],

    // def
    [
        "def - 1",
        '(do\n\
             (def inc (y) (+ 1 y))\n\
             (inc $ inc 2))',
        4
    ],

    [
        "def - 2 - multiple definitions",
        '(do\n\
             (def inc (y) (+ 1 y))\n\
             (def dec (y) (- y 1))\n\
             (dec $ inc 2))',
        2
    ],

    [
        "def - 3 - value",
        '(do\n\
             (def x (+ 1 2))\n\
             x)',
        3
    ],

    [
        "def - 3 - value",
        '(do\n\
             (def x 0)\n\
             (def x (+ 1 x))\n\
             (def x (+ 1 x))\n\
             x)',
        2
    ],

    [
        "def - 4 - scoping is lexical",
        '(do\
            (def f () x)\
            (let ((x 1)) (f)))',
        null,
        { error: true }
    ],

    [
        "sleep works",
        '(sleep 200)',
        200,
        { minTime: 200 }
    ],

    [
        "sleep returns value",
        '(+ (do (sleep 10) (+ 1 2))\n\
             3)',
        6
    ],

    [
        "sleep with multiple expression bodies",
        '(+ (do (sleep 10) (+ 1 2) (+ 3 4) (+ 5 6))\n\
             3)',
        14
    ],

    [
        "sleep returns timeout value - 1",
        '(sleep 10)',
        10
    ],

    [
        "sleep returns timeout value - 2",
        '(sleep (+ 1 2))',
        3
    ],

    [
        "sleep works with apply",
        '(apply sleep [(+ 1 2)])',
        3
    ],

    [
        "cycle",
        '(do\n\
             (def n (x) (if (> 30 x)\n\
                               (do\n\
                                 (sleep 1)\n\
                                 (n (+ 1 x)))\n\
                             x))\n\
             (n 0))',
        30
    ],

    [
        "factorial",
        '(do\n\
            (def fac (x)\n\
              (if (== 0 x)\n\
                  1\n\
                (* x $ fac $ - x 1)))\n\
            $ fac 4)',
        24
    ],

    [
        "cond - 1",
        '(cond (0 5) (0 3) (1 7))',
        7
    ],

    [
        "cond - 2",
        '(cond (1 5) (0 3) (1 7))',
        5
    ],

    [
        "cond - 3",
        '(cond (0 5) (2 3) (0 7))',
        3
    ],

    [
        "cond - 4",
        '(cond\n\
            ((empty [1]) 1) \n\
            ((empty []) 2) \n\
            ((empty []) 3))',
        2
    ],

    // arguments
    [
        "arguments - 1",
        '(def first-arg () (head args))\n\
         (first-arg 1 2 3)',
        1
    ],

    [
        "arguments length - 1",
        '(def len (lst)\n\
            (if (empty lst) 0\n\
                (+ 1 (len $ tail lst))))\n\
        (def arglen () (len args))\n\
        (arglen 1 2 3 1 2)',
        5
    ],

    // throw
    [
        "throw throws correct value",
        '(throw 1)',
        x => {
            return x == 1;
        },
        { error: true }
    ],

    [
        "try + throw",
        '(try (throw 1) (\\err -> (+ 1 err)))',
        2
    ],

    [
        "throw from begin",
        "(begin (throw 1))",
        x => x == 1,
        { error: true }
    ],


    [
        "throw from lambda",
        "((\\x -> (throw x)) 2)",
        x => x == 2,
        { error: true }
    ],

    [
        "throw from let",
        "(let ((x (throw 3))) 1)",
        x => x == 3,
        { error: true }
    ],


    [
        "try + lookup error",
        '(try x (\\err -> err))',
        x => assert(~unwrap(x).indexOf('lookup'))
    ],

    [
        "async/await - 1",
        '(await (async (+ 1 2)))',
        3
    ],

    [
        "async/await - 2",
        '(await (async (+ 1 2) (+ 3 4)))',
        7
    ],


    [
        "async/await - 3",
        '(def promise (async (+ 3 4)))\n\
         (await promise)',
        7
    ],

    [
        "await - 1",
        '(await (+ 3 4))',
        7
    ],

    [
        "par",
        '(par (+ 1 2) (+ 3 4) (/ 4 2))',
        lift([3, 7, 2])
    ],

    /* homoiconicity */

    [
        "eval - 1",
        '(eval [cons 1 [cons 2 []]])',
        lift([1, 2]),
    ],


    [
        "eval - 2",
        '(eval [+ 1 [- 9 4]])',
        6,
    ],

    [
        "quote - 1",
        '(quote (1 2))',
        lift([1, 2]),
    ],

    [
        "quote - 2",
        '(def x (tail (quote (1 2))))\n\
         (set x (cons 3 x))\n\
         (set x (cons + x))\n\
         (eval x)',
        5
    ],

    [
        "quote - 3 - literals should remain unchanged",
        '(+ (quote 1) 2)',
        3
    ],

    /* Objects */
    [
        "Object properties - 1",
        'a.b',
        1,
        { prelude: mkPrelude({ raw: { a: { b: 1 } } }) }
    ],

    [
        "Object properties - 2",
        'a.b.c',
        2,
        { prelude: mkPrelude({ raw: { a: { b: { c: 2 } } } }) }
    ],

    [
        "Object properties - 3",
        'a.b.c.d',
        3,
        { prelude: mkPrelude({ raw: { a: { b: { c: { d: 3 } } } } }) }
    ],


    [
        "Object properties - 4",
        '(a).b',
        1,
        { prelude: mkPrelude({ raw: { a: x => ({ b: 1 }) } }) }
    ],

    [
        "Object properties - 5",
        '(a).b',
        1,
        { prelude: mkPrelude({ raw: { a: x => ({ b: 1 }) } }) }
    ],

    [
        "Object properties - prefix dot - 1",
        '(. (a) "b")',
        1,
        { prelude: mkPrelude({ raw: { a: x => ({ b: 1 }) } }) }
    ],


    [
        "Object properties - prefix dot - 2",
        '(. (. (. a "b") "c") "d")',
        3,
        { prelude: mkPrelude({ raw: { a: { b: { c: { d: 3 } } } } }) }
    ],

    [
        "Object properties - 6",
        '(a.b).c',
        4,
        { prelude: mkPrelude({ raw: { a: { b: x => ({c: 4}) } } }) }
    ],

    [
        "Object properties - 7",
        '(. ((. a "b")) "c")',
        4,
        { prelude: mkPrelude({ raw: { a: { b: x => ({c: 4}) } } }) }
    ],

    [
        "Object properties - 8",
        '(a.b.c 0)',
        1,
        { prelude: mkPrelude({ raw: { a: { b: { c: x => x + 1 } } } }) }
    ],

    [
        "Object properties + lambda - 9",
        '(((\\x -> x) a.b.c) 0)',
        1,
        { prelude: mkPrelude({ raw: { a: { b: { c: x => x + 1 } } } }) }
    ],

    [
        "Object literals - 1",
        '{ a :1 \n\
           b: 2 }',
        lift({a:1, b: 2}),
    ],


    [
        "Object literals - 2",
        '{ a: 1 \n\
           a: 2 }',
        lift({a:2}),
    ],

    [
        "Object literals - nested - 3",
        '{ a: 1 \n\
           b: { c: 3 } }',
        lift({a:1, b: {c : 3}}),
    ],

    [
        "Object literals - nested - 4",
        '({ a: 1 \n\
            b: { c: (\\x -> (+ 1 x)) } }.b.c 99)',
        100
    ],

    [
        "Object literals - nested - 4",
        '{ a: { a: { a: {} b: {} } b: {} } }',
        lift({ a: { a: { a: {}, b: {} }, b: {} } })
    ],

    [
        "Object literals - nested, commas  - 5",
        '{ a: { a: { a: {}, b: {} }, b: {} } }',
        lift({ a: { a: { a: {}, b: {} }, b: {} } })
    ],

    [
        "Object literals + set - 1",
        '(def x {a:1})\n\
         (set x "a" 2) x',
        lift({a:2}),
    ],


    [
        "Object literals + set - 2",
        '(def x {a:1})\n\
         (set x.a 2) x',
        lift({a:2}),
    ],

    [
        "Object literals + set - 3",
        '(def x {a:{b:{c:{d:0}}}})\n\
         (set x.a.b.c.d (+ 1 x.a.b.c.d)) x',
        lift({a:{b:{c:{d:1}}}}),
    ],
];

describe("kiniro-lang", () => {
    var demos = {};
    tests.forEach(test => {
        it(test[0] /* + ":\n         " + test[1]
         + (['number', 'boolean'].includes(typeof test[2]) ? " => " + test[2] : "") */,
           function () {
               if (!test[3]) { test[3] = {}; };
               var options = test[3];
               const kinOptions = {};
               if (options.prelude) {
                   kinOptions.prelude = options.prelude;
               }

               var kiniro = new Kiniro(kinOptions), promise;

               if (!options.disableDemo) {
                   demos[test[0]] = test[1];
               }

               if (options.disabled) {
                   return;
               }

               if (!options.log) {
                   kiniro.logger = function () {};
               } else {
                   kiniro.logger = console.log;
               }

               if (!!options.echo) {
                   kiniro.echo = options.echo;
               }

               if (options.timeout) {
                   this.timeout(options.timeout);
               }

               promise = kiniro.run(test[1]);

               if (options.minTime) {
                   promise = new Promise((resolve, reject) => {
                       let flag = false;
                       setTimeout(() => flag = true, options.minTime);
                       promise.then(result => {
                           assert(flag, 'returned earlier than expected');
                           return result;
                       }).then(resolve).catch(reject);
                   });
               }

               if (options.error) {
                   return shouldFail(promise,
                                     typeof test[2] == 'function' ?
                                     test[2] : undefined);
               }

               if (typeof test[2] === 'function') {
                   return promise.then(test[2]);
               } else {
                   return promise.then(r => {
                       if (options.error) {
                           return;
                       }

                       if (options.show) {
                           console.log('EXPR----------------------\n',
                                       showExpression(r),
                                       '\n----------------------');
                       }

                       if (test[2] instanceof Array) {
                           assert.deepStrictEqual(r, test[2]);
                       } else {
                           assert.deepStrictEqual(unwrap(r), test[2]);
                       }

                   });
               }
           });
    });
});

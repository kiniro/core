module.exports = function (grunt) {
    /* Concatenation is used here to avoid unnecessary require
       boilerplate when browserifying. Using traditional require-style isn't
       appropriate here, since each module is small and uses stuff from
       ./src/const and ./src/misc.
     */
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.initConfig({
        mochaTest: {
            options: { reporter: 'spec' },
            src: ['test/*.js']
        },
        concat: {
            build: {
                src: [
                    /* contains symbolic constants */
                    'src/const.js',
                    /* helper functions used inside the interpreter */
                    'src/misc.js',
                    /* the main evaluate function */
                    'src/eval.js',
                    /* environment class */
                    'src/env.js',
                    /* lifting between JS and Kiniro values */
                    'src/lift.js',
                    /* definitions for core functions - absolutely minimal subset of a language */
                    'src/prelude.js',
                    /* KiniroCore class (that should be extended to actually use) */
                    'src/core.js',
                    /* defines what to export from module */
                    'src/module-exports.js',
                ],
                dest: 'index.js'
            }
        }
    });

    grunt.registerTask('build', ['concat:build']);
    grunt.registerTask('test', ['build', 'mochaTest']);
};

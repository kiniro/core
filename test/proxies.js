const assert = require('assert');
const fs = require('fs');
const parseRaw = require('@kiniro/parser');
const prelude = require('@kiniro/prelude');

const {
    KiniroCore,
    symbol,
    getType,
    unwrap,
    wrap,
    nil,
    parenth,
    cons,
    object,
    listToArray,
    arrayToList,
    seq,
    lift, unlift,
    types,
    keywords
} = require('../index.js');

class Kiniro extends KiniroCore {
    constructor () {
        super(...arguments);
        this.parseRaw = parseRaw;
        this.load(prelude);
    }
}

describe('value overriding', () => {
    const FunctionProxy = new Proxy(Function, {
        construct: () => {
            throw "CSP";
        }
    });

    const ObjectCreateStub = function (x) {
        if (x === Function || x === FunctionProxy)
            throw "Object.create(Function)";
        else
            return Object.create(...arguments);
    };

    it('FunctionProxy throws', () => {
        assert.throws(() => new FunctionProxy("x", "return x + 1"), x => x === "CSP");
    });

    it('blocks \'new Function\' calls', async () => {
        const kiniro = new Kiniro();
        kiniro.load({
            raw: {
                Function,
                new: function (func, ...args) {
                    if (typeof func.prototype !== 'undefined') {
                        return new func(...args);
                    } else {
                        throw "[new]: Not a constructor.";
                    }
                }
            },
            proxyMap: new Map([
                [Function, FunctionProxy]
            ])
        });

        await assert.rejects(async () => {
            await kiniro.exec('((new Function "x" "return x + 1") 2)');
        }, x => x === "CSP");
    });

    it('overrides literals', async () => {
        const kiniro = new Kiniro();
        kiniro.load({
            proxyMap: new Map([
                [2, 21]
            ])
        });

        const res = await kiniro.exec('(+ 2 2)');
        assert.deepStrictEqual(res, 42);
    });

    it('overrides computed values', async () => {
        const kiniro = new Kiniro();
        kiniro.load({
            proxyMap: new Map([
                [2, 42]
            ])
        });

        const res = await kiniro.exec('(+ 1 (+ 1 1))');
        assert.deepStrictEqual(res, 43);
    });

    describe("overrides lifted objects' properties", () => {
        it("allows to forbid 'eval', 'new Function' and 'Object.create(Function)'", async () => {
            const kiniro = new Kiniro();
            kiniro.load({
                proxyMap: new Map([
                    [Object.create, ObjectCreateStub],
                    [Function, FunctionProxy],
                    [eval, () => {
                        throw "Careful with That Axe, Eugene";
                    }]
                ])
            });
            kiniro.load({
                raw: {
                    Object, Function, jseval: eval,
                    new: function (func, ...args) {
                        if (typeof func.prototype !== 'undefined') {
                            return new func(...args);
                        } else {
                            throw "[new]: Not a constructor.";
                        }
                    },
                    evalBox1: { eval },
                    evalBox2: (() => {
                        const r = function () {
                        };
                        r.eval = eval;
                        return r;
                    })()
                }
            });

            await assert.rejects(
                async () => {
                    (await kiniro.exec('jseval'))("0");
                },
                x => x == "Careful with That Axe, Eugene",
                "Returns overriden value"
            );
            await assert.rejects(
                kiniro.exec('(Object.create Function)'),
                x => x == "Object.create(Function)",
                "Replaces native Object.create with a stub"
            );
            await assert.rejects(
                kiniro.exec('(new Function "x" "return x + 1")'),
                x => x == "CSP",
                "Forbids 'new Function'"
            );
            await assert.rejects(
                kiniro.exec('(jseval "() => {}")'),
                x => x == "Careful with That Axe, Eugene",
                "Forbids direct eval calls"
            );
            await assert.rejects(
                kiniro.exec('(evalBox1.eval "() => {}")'),
                x => x == "Careful with That Axe, Eugene",
                "Forbids eval calls in case of 'eval' being a property value of a lifted OBJECT"
            );
            await assert.rejects(
                kiniro.exec('(evalBox2.eval "() => {}")'),
                x => x == "Careful with That Axe, Eugene",
                "Forbids eval calls in case of 'eval' being a property value of a WRAPPED object"
            );
        });
    });
});

class KiniroCore {

    /** Create a new KiniroCore instance. This is an abstract class (can't be
        instantiated directly, must be extended before use). Class that extends
        KiniroCore must define this.parseRaw function which may be asynchronous.
        @param {Object} [opts] - opts.callStackSize determines how many .eval calls
        should be executed synchronously before starting with a new stack. opts.prelude
        is a prelude object - see KiniroCore.load for details */
    constructor (opts = {}) {
        if (this.constructor === KiniroCore) {
            throw new TypeError('Abstract class cannot be instantiated directly');
        }

        this.symbols = new Map(keywords);
        this.maxSyncCalls = opts.callStackSize || 1000;
        this.syncCalls = 0;
        this.env = new Env();
        this.proxyMap = new Map();
        this.proxy = (sth) => {
            if (this.proxyMap.has(sth)) {
                return this.proxyMap.get(sth);
            }
            return sth;
        };

        this.load(prelude);
        this.load(opts.prelude || {});
    }

    /** Load definitions from library object. This object should (may) contain
        properties named 'raw' and 'lifted' of type Object and 'proxyMap'
        property of type Map. Raw values will be lifted with 'lift' and lifted
        values will be 'wrap'ped before adding them to the environment, and
        'proxyMap' will be merged with this.proxyMap. Function returning such
        an object may be passed too. It will be called with 'this' value
        as first argument - this is done to make possible to reference kiniro
        object itself within environment or proxyMap. */
    load (lib) {
        if (typeof lib == 'function') {
            lib = lib(this);
        }

        const { lifted = {}, raw = {}, proxyMap = new Map() } = lib;

        // Put overrides to proxyMap BEFORE lifting anything
        proxyMap.forEach((value, key) => {
            this.proxyMap.set(key, value);
        });

        [[lifted, wrap], [raw, sth => lift(sth, this.proxy)]].forEach(([defs, f]) => {
            Object.keys(defs).forEach(name => {
                this.env.define(addSymbol(name, this.symbols), f(defs[name]));
            });
        });
    }

    /** Lookup a variable name in the environment. Returns unlifted value. Throws if no variable was found.
     @param name - variable name */
    lookup (name) {
        try {
            return unlift(this.env.lookup(this.symbols.get(name)), this.proxy);
        } catch (e) {
            throw `Couldn't lookup: ${name}`;
        }
    }

    /** Add a new definition to the environment.
        @param {string} name - name to be defined
        @param {any} value - Any JS value (will be lifted)
     */
    define (name, value) {
        this.env.define(addSymbol(name, this.symbols), lift(value, this.proxy));
    }

    /** Parse string into an AST.
        @param {string} src - code
        @param {Map} symbols - a Map from strings containing raw variable and keyword
        names to corresponding Symbols. Symbols are used to speedup the variable
        lookup process.
    */
    async parse (src, symbols) {
        const traverse = x => {
            if (x.type == 'symbol') {
                return symbol(addSymbol(x.value, symbols), x);
            } else if (x.type == 'prop') {
                return parenth([symbol(addSymbol('.', symbols)),
                                traverse(x.expr), wrap(x.prop.value)]);
            } else if (x.type == 'object') {
                return x.props.reduce((acc, [name,, expr]) => {
                    return parenth([symbol(addSymbol('set', symbols)),
                                    acc, wrap(name), traverse(expr)]);
                }, object({}));
            } else if (x.type == 'number') {
                return wrap(parseFloat(x.value), x);
            } else if (x instanceof Array) {
                return parenth(x.map(traverse));
            } else if (x.type.startsWith('kw_')) {
                return symbol(keywords.get(x.value), x);
            } else if (x.type === 'list') {
                return parenth([symbol(addSymbol('list', symbols)),
                                ...x.value.map(traverse)]);
            } else if (x.type === 'string') {
                return wrap(x.value);
            }
        };

        return (await this.parseRaw(src)).map(traverse);
    }

    choosePromise () {
        if (this.syncCalls > 0) {
            this.syncCalls--;
            return SyncPromise;
        } else {
            this.syncCalls = this.maxSyncCalls;
            return Promise;
        }
    }

    /** Evaluate AST */
    runAST (ast) {
        return new Promise((done, error) => {
            this.syncCalls = this.maxSyncCalls;
            this.evalMany(ast, this.env)
                .then(last)
                .then(done)
                .catch(error);
        });
    }

    /** Evaluate code.
        @param {string} src - code
        @return {Array} A lifted kiniro value (use {@link unlift} to extract).
        @see {@link KiniroCore#exec}
    */
    async run (src) {
        return await this.runAST(await this.parse(src, this.symbols));
    }

    /** Evaluate code.
        @param {string} src - code
        @return {any} A raw JS value.
        @see {@link KiniroCore#run}
    */
    async exec (src) {
        return this.run(src).then(sth => unlift(sth, this.proxy));
    }

    /** Evaluate a single expression (AST)
        @param expr - AST to evaluate
        @param env {Env} - Environment
    */
    eval (expr, env) {
        let promise = this.choosePromise();
        return new promise((resolve, reject) =>
                           evalFunction.call(this, expr, env, resolve, reject));
    }

    /** Evaluate a list of expressions (ASTs)
        @param list {array} - List of expressions
        @param env {Env} - Environment
    */
    evalMany (list, env) {
        let promise = this.choosePromise();
        return new promise((resolve, reject) =>
                           seq(list.map(expr => () => this.eval(expr, env)), resolve, reject));
    }
}

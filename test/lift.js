const {
    KiniroCore,
    symbol,
    getType,
    unwrap,
    wrap,
    nil,
    parenth,
    cons,
    object,
    listToArray,
    arrayToList,
    seq,
    lift, unlift,
    types,
    keywords
} = require('../index.js');
const assert = require('assert');

describe('lift() / unlift()', () => {

    describe('lift', () => {
        it('should satisfy assertions', () => {
            [
                [[], nil()],
                [1, wrap(1)],
                [{}, object({})],
                [{a:1}, object({ a: wrap(1) })],
                [{a:{b:1}}, object({ a: object({b:wrap(1)}) })],
                [null, wrap(null)],
            ].forEach(([input, expected]) => {
                assert.deepStrictEqual(lift(input), expected);
            });
        });

        it('should convert plain objects to OBJECTs', () => {
            assert.deepStrictEqual(lift({})[0], types.OBJECT);
            assert.deepStrictEqual(lift(Object.create(null))[0], types.OBJECT);
        });

        it('should convert functions to WRAPPED values', () => {
            assert.deepStrictEqual(lift(() => {})[0], types.WRAPPED);
            assert.deepStrictEqual(lift(function () {})[0], types.WRAPPED);
            assert.deepStrictEqual(lift(Object.create)[0], types.WRAPPED);
            assert.deepStrictEqual(lift(Object)[0], types.WRAPPED);
        });

        it('should preserve raw functions when lifting', () => {
            for (let f of [() => {}, function () {}, new Function ("x", ""), Object.create, Object]) {
                assert.deepStrictEqual(lift(f).raw, f);
            }
        });
    });

    it('function lifting', async () => {
        let lifted = unwrap(lift((x, y) => x + y));
        assert.deepStrictEqual(await lifted(wrap(1), wrap(2)), wrap(3));
    });

    it('unlift(lift(...))', async () => {
        const tests = [
            [1,2,3],
            [[1, 2], [3, [4]]],
            [],
            null,
            4,
            Object.create(null),
            function () {
                return [].reduce.call(arguments, (acc, i) => i + acc, 0);
            },
            async function () {
                let a = arguments;
                return await new Promise((resolve, reject) => {
                    setTimeout(() => {
                        resolve([].reduce.call(a, (acc, i) => i + acc, 0));
                    }, 1);
                });
            },
        ];

        for (let test of tests) {
            if (typeof test == 'function') {
                const argss = [[1, 2],
                               [3, 4]];
                for (let args of argss) {
                    assert.deepStrictEqual(await test.apply(null, args),
                                           await unlift(lift(test)).apply(null, args));
                }
            } else {
                assert.deepStrictEqual(unlift(lift(test)), test);
            }
        }
    });

    describe('circular references', () => {
        it('lift - 1', () => {
            const x = {};
            x.x = x;

            const y = [5];
            y.push({ x: y });
            assert.deepStrictEqual(lift(x), y);
        });

        it('lift - 2', () => {
            const x = {};
            const y = {};
            x.y = y;
            y.x = x;

            const lifted = [types.OBJECT, {}];
            lifted[1].y = [types.OBJECT, {}];
            lifted[1].y[1].x = lifted;
            assert.deepStrictEqual(lift(x), lifted);
        });
    });

    describe('objects', () => {
        class MyObject extends Object {
            constructor () {
                super();
                this.x = 1;
            }
        }

        it('lift() should lift direct instances of Object.getPrototypeOf({}) to OBJECT type', () => {
            assert.deepStrictEqual(getType(lift({})), types.OBJECT);
        });

        it('lift() should lift indirect instances of Object.getPrototypeOf({}) to WRAPPED type', () => {
            assert.deepStrictEqual(getType(lift(new MyObject())), types.WRAPPED);
        });

        it('lift() should lift Objects whose prototype is null to OBJECTs', () => {
            assert.deepStrictEqual(getType(lift(Object.create(null))), types.OBJECT);
        });
    });
});

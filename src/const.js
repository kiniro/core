// Expression types
// SYMBOL is a variable name
// PARENTH is a list of other expressions enclosed in parentheses
// WRAPPED is any other value, including numerals, strings, etc.
const SYMBOL = 0,
      PARENTH = 1,
      NIL = 2,
      CONS = 3,
      WRAPPED = 4,
      OBJECT = 5;

const KEYWORD_LAMBDA = Symbol('lambda'),
      KEYWORD_BEGIN = Symbol('begin'),
      KEYWORD_APPLY = Symbol('apply'),
      KEYWORD_DEFINE = Symbol('def'),
      KEYWORD_LET = Symbol('let'),
      KEYWORD_IF = Symbol('if'),
      KEYWORD_COND = Symbol('cond'),
      KEYWORD_THROW = Symbol('throw'),
      KEYWORD_TRY = Symbol('try'),
      KEYWORD_SET = Symbol('set'),
      KEYWORD_ARGS = Symbol('args'),
      KEYWORD_PAR = Symbol('par'),
      KEYWORD_ASYNC = Symbol('async'),
      KEYWORD_AWAIT = Symbol('await'),
      KEYWORD_QUOTE = Symbol('quote'),
      KEYWORD_EVAL = Symbol('eval'),
      KEYWORD_DOT = Symbol('.');

/** Mapping from string representations of keywords to their corresponding
   Symbols. Used on parse stage. */
const keywords = new Map([
    ['\\', KEYWORD_LAMBDA],
    ['begin', KEYWORD_BEGIN],
    ['apply', KEYWORD_APPLY],
    ['def', KEYWORD_DEFINE],
    ['let', KEYWORD_LET],
    ['if', KEYWORD_IF],
    ['cond', KEYWORD_COND],
    ['throw', KEYWORD_THROW],
    ['try', KEYWORD_TRY],
    ['set', KEYWORD_SET],
    ['args', KEYWORD_ARGS],
    ['par', KEYWORD_PAR],
    ['async', KEYWORD_ASYNC],
    ['await', KEYWORD_AWAIT],
    ['quote', KEYWORD_QUOTE],
    ['eval', KEYWORD_EVAL],
    ['.', KEYWORD_DOT],
]);

const ERR_ILLEGAL_TYPE = "illegal expression type.",
      ERR_CANT_EVAL = "couldn't eval expression",
      ERR_INCORRECT_SYNTAX = "incorrect syntax",
      ERR_COND = "cond: none of the conditions is true",
      ERR_EMPTY_PARENTHESES = "empty parentheses";

const SyncPromise = require('synchronous-promise').SynchronousPromise;

/** Create a symbol with given name
   @param {string} value - Name
   @param {Object} [meta={}] - some info from parse phase (i.e. symbol's token).
 */
const symbol = (value, meta) =>
      [SYMBOL, value, meta || {}];

/** Get an expression type */
const getType = sth => sth[0];

/** Get the value of an expression */
const unwrap = sth => sth[1];

/** Wrap a raw value
   @param value {any} - Raw value
 */
const wrap = value => [WRAPPED, value];

/** Lift an object non-recursively.
    @param value {any} - object */
const object = value => [OBJECT, value];

const exprToList = expr => {
    const type = getType(expr);
    if (PARENTH === type) {
        const value = unwrap(expr);
        return arrayToList(value.map(exprToList));
    } else {
        return expr;
    }
};

const listToExpr = expr => {
    const type = getType(expr);
    if (CONS === type) {
        return parenth(listToArray(expr).map(listToExpr));
    } else {
        return expr;
    }
};

/** Construct a NIL object */
const nil = () => [NIL];

/** Construct parentheses from list */
const parenth = arr => [PARENTH, arr];

/** Construct a CONS (no pun intended). It can be used not only to build lists,
    but also to construct arbitrary binary trees.
    @param x {*} - head element
    @param xs {Object} - rest of the list. Use nil() as the end of a list. */
const cons = (x, xs) => [CONS, [x, xs]];

/** Returns the last element of a given Array. If Array length is 0, returns nil().
   @param lst {Array}
 */
const last = lst => lst.length ? lst[lst.length - 1] : nil();

/** Add new symbol to lookup table. Used by 'parse'.
   @param name - symbol's value
   @param symbols - lookup table (see 'keywords') */
const addSymbol = (name, symbols) => {
    if (symbols.has(name)) {
        return symbols.get(name);
    } else {
        let sym = Symbol(name);
        symbols.set(name, sym);
        return sym;
    }
}

/** Convert CONS/NIL list into JS Array */
function listToArray (lst) {
    var r = [];
    while (getType(lst) == CONS) {
        if ('r' in unwrap(lst)) {
            r = r.concat(unwrap(lst).r.slice(0, unwrap(lst).i + 1).reverse());
            break;
        } else {
            r.push(unwrap(lst)[0]);
            lst = unwrap(lst)[1];
        }
    }
    return r;
}

/** Convert JS Array into CONS/NIL list */
const arrayToList =
    array => array.length ?
           array.slice().reverse().reduce((x, y) => cons(y, x), nil()) : nil();

/** Executes a list of promises sequentially (but is not a Promise itself). This function accepts callbacks.
    @param arr {Array} - Array of functions that return Promise.
    @param resolve {function} - callback
    @param reject {function} - error callback
*/
const seq = (arr, resolve, reject) => {
    const r = [];
    function iter (arr) {
        if (arr.length) {
            arr[0]().then(result => {
                r.push(result);
                iter(arr.slice(1));
            }).catch(reject);
        } else {
            resolve(r);
        }
    }
    iter (arr);
};

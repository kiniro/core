const assert = require('assert');
const { seq } = require('../index.js');
const SyncPromise = require('synchronous-promise').SynchronousPromise;

describe('seq', () => {
    it('should work with Promise', () => {
        return new Promise((resolve, reject) => {
            seq([
                async () => await 0,
                async () => await 1,
            ], result => {
                assert.deepStrictEqual(result, [0, 1]);
                resolve();
            }, reject);
        });
    });

    it('should work with SyncPromise', () => {
        return new SyncPromise((resolve, reject) => {
            seq([
                () => new SyncPromise((resolve, reject) => resolve(0)),
                () => new SyncPromise((resolve, reject) => resolve(1)),
            ], result => {
                assert.deepStrictEqual(result, [0, 1]);
                resolve();
            }, reject);
        });
    });
});
